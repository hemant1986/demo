<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment', function (Blueprint $table) {
            $table->bigIncrements("recid");
            $table->unsignedBigInteger("event_type")->nullable();
            
            $table->integer("event_min",0)->nullable(); //IN CASE IF EVENT TYPE DURATION IS CHANGED, KEEPING FOR PAST REFERENCE
            $table->date("date",0)->nullable();
            $table->time("start")->nullable();
            $table->time("end")->nullable();
            $table->unsignedBigInteger("user")->nullable();
            $table->string("firstname",128)->nullable();
            $table->string("lastname",128)->nullable();
            $table->string('email',128)->nullable();

            $table->foreign("event_type")->references("recid")->on("event_types")->onDelete("cascade");
            $table->foreign("user")->references("id")->on("users")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("appointment");
    }
}
