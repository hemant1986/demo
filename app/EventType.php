<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    protected $primaryKey = 'recid';

    public function user()
    {
        return $this->belongsTo("App\User","user");
    }
}
