<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
    protected $primaryKey = "recid";

    public function event_type()
    {
        return $this->belongsTo("App\EventType","event_type");
    }

    public function user()
    {
        return $this->belongsTo("App\User","user");
    }
}
