<?php
namespace App\Http\Traits;
use Illuminate\Contracts\Encryption\DecryptException;
use App\User;

trait CheckUsers
{

    protected function checkUser($userid)
    {
        return $this->unobfuscate($userid);
    }

    protected function getUser($enckey)
    {
        $user = NULL;
        $userid = $this->checkUser($enckey);

        if($userid !== NULL)
        {
            $user = User::select("id","name","email")->where("id",$userid);

            if($user->exists())
            {
                $user = $user->first();
            }
        }

        return $user;


    }   

    protected function obfuscate($userid)
    {
        
        return encrypt($userid);
    }

    protected function unobfuscate($userid)
    {
        $d_userid = NULL;

        try{
            $d_userid = decrypt($userid);
        }catch(DecryptException $e) {}

        return $d_userid;
    }
}
?>