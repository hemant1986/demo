<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventType;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Auth;

class EventTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    //Lists all event type 
    public function show()
    {   
        $event_types = EventType::all();
        return view("event_type.show")->with("event_types",$event_types);
        
    }
    //NEW FUNCTION >> DISPLAYS USER INTERFACE
    public function new()
    {
        $event_type = new EventType();

        //EDIT MODE FILE IS SAME IF THE ID IS PRESENT IT WILL BE IN UPDATE MODE ELSE STORE / CREATE MODE
        return view("event_type.edit")->with("event_type",$event_type);
    }


    public function getValidation(Request $request)
    {
        $rules = array();
        $rules["event_name"] = "required";

        $unique_rule = "unique:event_types,event_name,".$request->input("hidref").",recid,user,".Auth::user()->id;
      
        $rules["event_name"] .= "|".$unique_rule;
        $rules["event_min"] = "required|numeric";

        

        $custom_messages = array();
        $custom_messages["required"] = ":attribute is required";
        $custom_messages["numeric"] = ":attribute must be numeric";
        $custom_messages["unique"] = ":attribute already taken.";
        
        $validation = Validator::make($request->all(),$rules,$custom_messages);

        return $validation;
    }

    //Stores the entry in the database
    public function save(Request $request)
    {
       
        $validator = $this->getValidation($request);

        $event_type = new EventType();
        $event_type->recid = $request->input("hidref");
        $event_type->event_name = $request->input("event_name");
        $event_type->event_min = $request->input("event_min");
        $event_type->user = Auth::user()->id;

         if($validator->fails())
         {
             
             return view("event_type.edit")->with("event_type",$event_type)->withErrors($validator->messages());
         }
         else
         {
            
            if($request->input("hidref") !== NULL)
            {
                $event_type = EventType::find($request->input("hidref"));

                if(!$event_type->exists())
                {
                    return redirect(route("home")); 
                }
            }  

            $event_type->event_name = $request->input("event_name");
            $event_type->event_min = $request->input("event_min");  
            $event_type->save();
            
            return redirect(route("home"));
         }
    }

    //Displays the ui for editing event type
    public function edit(Request $request,$id)
    {
        $event_type = EventType::where("recid",$id);

        if($event_type->exists())
        {
            $event_type = $event_type->first();
            return view("event_type.edit")->with("event_type",$event_type);
        }
        else
        {
            return redirect(route("home"))->with("msg",["type"=>"error","message"=>"There was a problem locating Event Type"]);
        }

    }  
}
