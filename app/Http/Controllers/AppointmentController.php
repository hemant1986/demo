<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventType;
use App\Appointment;
use App\User;
use App\Http\Traits\CheckUsers;

class AppointmentController extends Controller
{
    use CheckUsers;

    public function _construct()
    {
        $this->middleware("guest");
    }

    public function create(Request $request,$userid)
    {
        $user = $this->getUser($userid);

        if($user !== NULL)
        {
            $event_types = EventType::select("recid","event_name","event_min")->where("user",$user->id)->orderBy("event_name")->get();
            
         

            $pagedata = array();
            $pagedata["event_types"] = $event_types;
            $pagedata["user"] = $user;
            $pagedata["enc_key"] = $this->obfuscate($user->id);

            return view("appointment.create")->with($pagedata);
           
        }
        
        //
    }

    public function book(Request $request,$userid)
    {
        $user = $this->getUser($userid);
       
        $response = array();
        $response["success"] = false;
        
        if($user !== NULL)
        {
            $start = date_create_from_format("h:i a",$request->input("start"));
            $start = date_format($start,"H:i:0");

            $end = date_create_from_format("h:i a",$request->input("end"));
            $end = date_format($end,"H:i:0");

            $appointment  = new Appointment();
            $appointment->event_type = $request->input("event_type");
            $appointment->event_min = $request->input("event_min");
            $appointment->date = $request->input("date"); //FORMATTED IN Y-m-d
            $appointment->start = $start;
            $appointment->end = $end;
            $appointment->user = $user->id;
            $appointment->firstname = $request->input("firstname");
            $appointment->lastname = $request->input("lastname");
            $appointment->email = $request->input("email");

            $appointment->save();

            if($appointment->recid > 0)
            {
                $response["success"] = true;
            }
        }

        return json_encode($response);
    }

    public function freeSlot(Request $request,$userid)
    {
        //$userid = decrypt($userid);

        //$user = User::select("id","name","email")->where("id",$userid);
        $user = $this->getUser($userid);

        if($user !== NULL)
        {
            $pagedata = array();
            $pagedata["freeslots"] = [];
            $pagedata["date"] = $request->input("date");
            $pagedata["duration"] = $request->input("event_min");
           
            
            $date_timestamp = strtotime($pagedata["date"]);

        
            $current_date_ts = date_create();
            date_time_set($current_date_ts,date_format($current_date_ts,"H"),date_format($current_date_ts,"i"),0);

            

            $date_ts = date_create();
            date_timestamp_set($date_ts,$date_timestamp);
            date_time_set($date_ts,10,0,0);

            $pagedata["date_display"] = date_format($date_ts,"l, F d Y");

            $end_date_ts = date_create();
            date_timestamp_set($end_date_ts,date_timestamp_get($date_ts));
            date_time_set($end_date_ts,19,0,0);
            date_modify($end_date_ts,"-".$pagedata["duration"]."mins");

            while($date_ts <= $end_date_ts)
            {
                
                $copy_date_ts = date_create();
                $copy_date_ts = date_timestamp_set($copy_date_ts,date_timestamp_get($date_ts));

                $start_db = date_format($date_ts,"H:i:0");
                $date_ts = date_modify($date_ts,"+15 min");
                $end_db = date_format($date_ts,"H:i:0");

                if($date_ts > $current_date_ts)
                {
                   // echo "date='".$pagedata["date"]."' and (end >= '".$start_db."' and start <= '".$end_db."')";
                    $appointment_set = Appointment::whereRaw("user = '".$user->id."' and date='".$pagedata["date"]."' and (end > '".$start_db."' and start <= '".$end_db."')");

                    if($appointment_set->exists()) {}
                    else
                    {
                        array_push($pagedata["freeslots"],date_format($copy_date_ts,"h:i a"));
                    }
                
                }
                
                    
            }


            return view("appointment.freeslot")->with($pagedata);
        }
    }

    public function checkSlot(Request $request,$userid)
    {
        //$userid = decrypt($userid);

        //$user = User::select("id","name","email")->where("id",$userid);
        $user = $this->getUser($userid);

        if($user !== NULL)
        {
            $pagedata = array();
            $pagedata["notavailable"] = [];
            $pagedata["date"] = $request->input("date");
          
        
            $slot_check_month = date_create();
            date_timestamp_set($slot_check_month,strtotime($pagedata["date"]));

            $current_date_ts = date_create();
            date_time_set($current_date_ts,date_format($current_date_ts,"H"),date_format($current_date_ts,"i"),0);

       
            for($i=1;$i<=date_format($slot_check_month,"t");$i++)
            {
            
                $date_ts = date_create();
                date_timestamp_set($date_ts,date_timestamp_get($slot_check_month));
                date_time_set($date_ts,10,0,0);

                $end_date_ts = date_create();
                date_timestamp_set($end_date_ts,date_timestamp_get($date_ts));
                date_time_set($end_date_ts,19,0,0);
                date_modify($end_date_ts,"-15 min");
                $not_available = true;

                while($date_ts <= $end_date_ts)
                {
                    
                    $copy_date_ts = date_create();
                    $copy_date_ts = date_timestamp_set($copy_date_ts,date_timestamp_get($date_ts));

                    $start_db = date_format($date_ts,"H:i:0");
                    $date_ts = date_modify($date_ts,"+15 min");
                    $end_db = date_format($date_ts,"H:i:0");

                    if($date_ts > $current_date_ts)
                    {
                      //  echo "<br> date='".date_format($slot_check_month,"Y-m-d")."' and (end > '".$start_db."' and start <= '".$end_db."'";
                        $appointment_set = Appointment::whereRaw("user='".$user->id."' and date='".date_format($slot_check_month,"Y-m-d")."' and (end > '".$start_db."' and start <= '".$end_db."')");

                        if($appointment_set->exists()) 
                        {
                            
                            //echo "true";
                            //THIS DAY IS OCCUPIED LEAVE TO THE NEXT DAY
                            //break;
                        }
                        else
                        {
                           
                            $not_available = false;
                        }
                    
                    }
                }

                if($not_available)
                {
                    array_push($pagedata["notavailable"],date_format($slot_check_month,"Y-n-d"));
                }

                date_modify($slot_check_month,"+1 day");
            }
            return json_encode($pagedata);
        }
    }
}
