<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventType;
use App\Appointment;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $pagedata = array();
        $pagedata["view_dashboard"] = $request->input("view_dashboard");
        $pagedata["schedule_event_type"] = $request->input("schedule_event_type");

        if(!isset($pagedata["view_dashboard"]))
        {
            $pagedata["view_dashboard"] = "event_type";
        }
        
        if($pagedata["view_dashboard"]== "event_type")
        {
            $pagedata["event_types"] = EventType::where("user",Auth::user()->id)->orderBy("event_name")->get();
        }
        else
        {
            if(!isset($pagedata["schedule_event_type"]))
            {
                $pagedata["schedule_event_type"] = "upcoming";
            }
            
            $date = date_create();
            $date_str = date_format($date,"Y-m-d");
            $current_hour  = date_format($date,"H:i:0");

           

            

            
            $op = ">"; //upcoming more than current time
            $orderby = "asc";

            if($pagedata["schedule_event_type"] == "past") 
            {
                $op = "<= "; //past less than the current_time
                $orderby = "desc";
            }
            
            $sql_frag = "timestamp(date,start) $op '".$date_str." ".$current_hour."' and user='".Auth::user()->id."'";

            $appointments = Appointment::whereRaw($sql_frag);
            $appointments =  $appointments->with(["event_type","user"])->orderBy("date",$orderby)->orderBy("start",$orderby)->paginate(5)->appends(["view_dashboard"=>$pagedata["view_dashboard"],"schedule_event_type"=>$pagedata["schedule_event_type"]]);

            $pagedata["appointments"] = $appointments;
        }

       

        return view('home')->with($pagedata);
    }
}
