<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
 //   return view('home');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');


//EVENT TYPE LISTING FOR FUNCTION 
//GET ROUTES 
//Route::get("/event_type/show","EventTypeController@show")->name("event_type.show");
Route::get("/event_type/new","EventTypeController@new")->name("event_type.new");
Route::get("/event_type/edit/{ID}","EventTypeController@edit")->name("event_type.edit");

//SAVE RELATED OP
Route::post("/event_type/save","EventTypeController@save")->name("event_type.save");


Route::get("/logout",function()
{
    Auth::logout();
    return redirect(route("login"));
});

Route::get("/appointment/create/{id}","AppointmentController@create")->name("appointment.create");
Route::get("/appointment/free-slot/{id}","AppointmentController@freeSlot")->name("appointment.free-slot");

Route::post("/appointment/book/{id}","AppointmentController@book")->name("appointment.book");
Route::get("/appointment/check-slot/{id}","AppointmentController@checkSlot")->name("appointment.check-slot");