@extends("layout.layout")
@section("content")
@php $context="event_type" @endphp
@include("menu.context")
<script language="javascript">
    $(function()
    {
        $(".minute-layout .minute-item").click(function()
        {
            $(this).parents(".minute-layout").find(".minute-item.active").removeClass("active");
            $(this).addClass("active");

            if($(this).attr("data-minute") == "custom")
            {

            }
            else
            {
                $("#event_min").val($(this).attr("data-minute"));

                //console.log($("#event_min").val());
            }
        })
    })

    function dovalidate()
    {
        if(document.frmgeneral.checkValidity())
        {
            if($(".minute-layout .minute-item[data-minute=custom]").hasClass("active"))
            {
                $("#event_min").val($("#custom_minute").val());
            }

            return true;
        }

        return false;
    }
</script>
<div class="row justify-content-center">
  
    <div class="col-md-8">
        @include("common.msg")
        <div class="card">
            <div class="card-body">
                
                <form method="post" name="frmgeneral" action="{{ route("event_type.save") }}" autocomplete="off">
                    @csrf
                    <input type="hidden" name="hidref" value="{{ $event_type->recid }}"/>

                    <div class="form-group">
                        <label for="event_type">Event Name *</label>
                        <div class="input-group">
                            <input type="text" class="form-control col-md-6 @error('event_name') is-invalid @enderror "" name="event_name" required value="{{ $event_type->event_name }}"/>

                            @error("event_name")
                                <span class="invalid-feedback d-block" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="event_min">Event Duration *</label>
                        <input type="hidden" name="event_min" id="event_min" value="{{ $event_type->event_min}}"/>


                        <div id="minute_selector" class="minute-layout">
                            @php $minute_option=[15,30,45,60] @endphp
                            @php $is_custom_input="active" @endphp
                            

                            @foreach($minute_option as $minute)
                                
                                @php $active="" @endphp
                                @if($event_type->event_min == $minute)
                                    @php $active="active" @endphp
                                    @php $is_custom_input="" @endphp
                                @endif  
                                <div class="minute-item {{$active}}" data-minute="{{ $minute }}">
                                    <div class="line1">{{$minute}}</div>
                                    <div class='line2 text-muted'>min</div>
                                </div>
                            @endforeach

                            @php $custom_input="" @endphp 
                            @if ($is_custom_input == "active")
                                @php $custom_input=$event_type->event_min @endphp
                            @endif 
                            <div class="minute-item {{ $is_custom_input}}"  data-minute="custom">
                                <input type="text" class="custom-input" id="custom_minute" name="custom_minute" value="{{ $custom_input }}"  pattern="[0-9]+"/>
                                <div style="font-size:11px;" class='text-muted'>min</div>
                            </div>
                            
                        </div>

                        @error("event_min")
                            <div class="invalid-feedback d-block" role="alert"><strong>{{ $message }}</strong></div>
                        @enderror
                    </div>
                    
                    <hr/>
                    <div class="text-right">
                        <a href="{{ route("home") }}" class="btn btn-default">Back</a>
                        <button type="submit" value="Save" name="cmdsave" class="btn btn-primary" onclick="javascript:return dovalidate();">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
   
</div>
@endsection