
<div class="row">   
    <div class="col-md-9">
        <div>My Link</div>
        <div><a href="{{ route("appointment.create",encrypt(Auth::user()->id)) }}">Scheduler</a></div>
    </div>
    <div class="col-md-3">
        <a href="{{ route("event_type.new") }}" class="btn btn-flat">New Event Type</a>
    </div>
</div>

    @if(count($event_types))
        <div class="row">
            @foreach($event_types as $event_type)
                <div class="col-md-4">
                    <div class="card dashboard-card">
                        <div class="card-body">
                            <div class="item-title">{{ $event_type->event_name }}</div>
                            <div class="text-muted">{{ $event_type->event_min }} min</div>
                        </div>
                        <div class="card-action">
                            <a href="{{ route("event_type.edit",$event_type->recid) }}">Edit</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body text-center">
                        <i class="fa fa-exclamation-triangle fa-lg"></i>
                        <div class="text-muted">No Event Types Available</div>
                    </div>
                <div>
            </div>
        </div>
    @endif


