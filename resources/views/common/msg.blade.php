@if(isset($msg))
<div class="alert alert-{{ $msg["type"] }}">
    {{ $msg["message"] }} 
</div>
@endif