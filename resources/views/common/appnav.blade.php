<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">&nbsp;{{ env("APP_NAME") }}</a>
  @guest
    <ul class="navbar-nav  ml-auto">
        <li class="nav-item">
          <a href="{{ route("login") }}" class="nav-link">Login</a>
        </li>
        <li class="nav-item">
          <a href="{{ route("register") }}" class="nav-link">Register</a>
        </li>
    </ul>
  @endguest

  @auth
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-user-circle fa-2x float-left profile-icon"></i> {{ auth()->user()->name}}
            </a>
            <div class="dropdown-menu profile-dropdown">
              <a class="dropdown-item" href="{{ route("logout") }}">Logout</a>
            </div>
        </li>
    </ul>
  @endauth
</nav>
