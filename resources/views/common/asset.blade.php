<link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.min.css')}}"/>
<script language="javascript" src="{{asset('js/jquery-3.5.0.min.js')}}"></script>
<script language="javascript" src="{{asset('js/popper.min.js')}}"></script>
<script language="javascript" src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>

<link rel="stylesheet" href="{{ asset('css/fontawesome_5.13/css/all.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('css/app.css') }}"/>




<meta name="csrf-token" content="{{ csrf_token() }}" />

<script language="javascript">
    //VALIDATING POST REQUEST
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>