@extends('layout.layout')

@section('content')
@php $context="dashboard" @endphp
@include("menu.context")
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8 schedule-event">
            @if($view_dashboard == "event_type")
                @include("event_type.show")
            @else
                <div class="row justify-content-right">
                    <div class="col-md-12 text-right" style="padding:20px;"><span class="text-muted">Displaying {{ $appointments->currentPage()  }} / {{ $appointments->lastPage() }} </span></div>

                </div>
                <div class="card">
                    <div class="card-header no-padding-bottom">
                        <ul class="nav nav-tabs" >
                            <li class="nav-item">
                                @php $active="" @endphp 
                                @if($schedule_event_type=="upcoming")
                                    @php $active="active" @endphp
                                @endif 
                                <a class="nav-link {{$active}}" href="{{ route("home","view_dashboard=scheduled_events&schedule_event_type=upcoming") }}">Upcoming</a>
                            </li>
                            <li class="nav-item">
                                @php $active="" @endphp
                                @if($schedule_event_type=="past")
                                    @php $active="active" @endphp
                                @endif 
                                <a class="nav-link {{$active}}" href="{{ route("home","view_dashboard=scheduled_events&schedule_event_type=past") }}">Past</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body event-container">
                        @include("appointment.show")
                    </div>
                </div>

                <div class="row justify-content-center spacing-top">
                    <div class="col-md-12 text-center">
                        {{ $appointments->links()}}
                    </div>
                </div>
            @endif 
        </div>
    </div>
</div>
@endsection
