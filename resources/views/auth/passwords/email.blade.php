@extends('layout.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3 class="text-center text-muted spacing-top" >{{ __('Reset Password') }}</h3>

            @if (session('status'))
                <p class="alert alert-success">{{ session('status') }}</p>
            @else
                <div class="card spacing-top">
                    <div class="card-body">
                    
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group">
                                <label for="email">{{ __('E-Mail Address') }}</label>

                            
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                            </div>

                            <div class="form-group">
                            
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                                
                            </div>
                        </form>
                    </div>
                </div>
            @endif
            
        </div>
    </div>
</div>
@endsection
