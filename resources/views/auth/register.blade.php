@extends('layout.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h3 class="text-center text-muted spacing-top" >{{ __('Signup') }}</h3>
            <div class="card spacing-top">
                

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" autocomplete="off">
                        @csrf
                        
                         <div class="form-group ">
                            <label for="email">{{ __('Enter your email address to get started') }}</label>

                           
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                         
                        </div>

                        <div class="form-group">
                            <label for="name">{{ __('Enter your full name') }}</label>

                            
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                          
                        </div>

                       

                        <div class="form-group">
                            <label for="password">{{ __('Choose a password with at least 8 characters') }}</label>

                            
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required pattern=".{8,}" autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                           
                        </div>

                        <div class="form-group ">
                            <label for="password-confirm" >{{ __('Confirm Password') }}</label>

                           
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            
                        </div>

                        <div class="form-group ">
                           
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Continue') }}
                                </button>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
