@extends("layout.appt_layout")
@section("content")
    <script type="text/babel">
        class Appointment extends React.Component
        {
            constructor(props)
            {
                super(props);

                props.userinfo = JSON.parse(props.userinfo);
                props.event_types = JSON.parse(props.event_types);
                this.setEventType = this.setEventType.bind(this);
                this.initCalendar = this.initCalendar.bind(this);
                this.moveBack = this.moveBack.bind(this);

                this.state = {
                    step:"step1",
                    appointmentdata:{}
                }
            }

            moveBack(event)
            {
                var self = this;
                var back_step = $(event.currentTarget).attr("data-back-step");

                self.setState({
                    step:back_step
                })
            }
           

            setEventType(event)
            {
                var self = this;
                var next_step = $(event.currentTarget).attr("data-next-step");

                if(next_step == "step1")
                {
                    this.setState({
                        step:"step1"
                    })

                    $("#calendar_timeslot").empty().addClass("d-none");
                    $("#calendar").removeClass().addClass("col-md-12")

                    document.frminput.reset();
                    $(document.frminput).find("input").each(function()
                    {
                        $(this).removeClass("is-invalid");
                        
                    
                    })
                }
                else if(next_step == "step2")
                {
                    var event_name = $(event.currentTarget).attr("data-event-name");
                    var event_min = $(event.currentTarget).attr("data-event-min");
                    var event_type = $(event.currentTarget).attr("data-event-type");

                    this.setState({
                        step:"step2",
                        event_name:event_name,
                        event_min:event_min,
                        event_type:event_type
                    })

                    self.initCalendar(event);

                }
                else if(next_step == "step4")
                {
                    if(this.props.is_saving)
                    {
                        alert("Please wait");
                        return;
                    }
                    var book_url = this.props.book_url

                    
                    var savedata = this.state;
                    savedata["firstname"] = document.frminput.firstname.value;
                    savedata["lastname"] = document.frminput.lastname.value;
                    savedata["email"] = document.frminput.email.value;


                    if(document.frminput.checkValidity())
                    {

                        $.ajax({
                            url:book_url,
                            type:"POST",
                            cache:false,
                            dataType:"json",
                            data:savedata,
                            success:function(response)
                            {

                                self.props.is_saving = false;

                                if(response.success)
                                {
                                    self.setState({
                                        step:"step4"
                                    })
                                }
                                else
                                {   
                                    alert("There was a problem in booking appointment");
                                }
                            }
                        })
                    }
                    else
                    {
                        $("[name=frminput] input").each(function()
                        {
                            $(this).removeClass("is-invalid");

                            if($(this).val() == "")
                            {
                                $(this).addClass("is-invalid");
                            }
                        })
                    }

                    event.stopPropagation();
                    event.preventDefault();
                    return false;

                }
            }

            initCalendar(event)
            {
                var self = this;

                makeCalendar(self);
            }

            setEventTimeSlot(element,date)
            {
                this.setState({
                    step:"step3",
                    date:date,
                    start:$(element).attr("data-start"),
                    end:$(element).attr("data-end"),
                    date_display:$(element).attr("data-date-display"),
                    duration:$(element).attr("data-duration")
                })
            }

            render()
            {
                return(
                    <div>
                        <div className={(this.state.step=="step1"?"d-block":"d-none")}>
                            <div class="appointment_step1">
                                <h3 className="text-center text-muted">
                                    {this.props.userinfo.name}
                                </h3>
                                <div className="text-muted">
                                    <div>
                                        Welcome to my scheduling page. Please follow the instructions to add event to my calendar.
                                    </div>
                                </div>
                                
                                <div class="row justify-content-left">
                                    <div class="list-group spacing-top col-md-8 schedule-list">
                                        {this.props.event_types.map((event_type,index)=>
                                            
                                                
                                            <a href="javascript:;" onClick={this.setEventType} data-next-step="step2" data-event-min={event_type.event_min} data-event-name={event_type.event_name} data-event-type={event_type.recid} className="list-group-item list-group-item-action">
                                                <i class="fa fa-circle fa-lg float-left item-bullet"></i>
                                                <i class="fa fa-caret-right fa-lg float-right"></i>
                                                {event_type.event_name} 
                                            </a>
                                            
                                        )}
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <div className={(this.state.step=="step2"?"d-block":"d-none")}>
                            <div class="appointment_step2">
                                <div className="row">
                                    <div className="col-md-3">
                                        <a class="btn btn-link" onClick={this.moveBack} data-back-step="step1"><i class="fa fa-arrow-left text-primary"></i></a>
                                        <div class="step_description">
                                            <div className="text-muted"><small>{this.props.userinfo.name}</small></div>
                                            <h4>{this.state.event_name}</h4>
                                            <div><span className="text-muted"><i class="fa fa-clock text-muted"></i> {this.state.event_min} mins</span></div>
                                        </div>
                                    </div>
                                    <div className="col-md-9" >
                                        <div className="row">
                                            <div id="calendar" className="col-md-12"></div>
                                            <div id="calendar_timeslot" className="col-md-4 d-none"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={(this.state.step=="step3"?"d-block":"d-none")}>
                            <div className="row">
                                <div className="col-md-4">
                                    <a class="btn btn-link" onClick={this.moveBack} data-back-step="step2"><i class="fa fa-arrow-left text-primary"></i></a>
                                    <div class="step_description">
                                        <div className="text-muted"><small>{this.props.userinfo.name}</small></div>
                                        <h4>{this.state.event_name}</h4>
                                        <div><span className="text-muted"><i class="fa fa-clock"></i> {this.state.event_min} mins</span></div>
                                        <div><span className="text-green"><i class="fa fa-calendar-o"></i> {this.state.start} - {this.state.end} {this.state.date_display}</span></div>
                                    </div>
                                </div>
                                <div className="col-md-8" >
                                    
                                    <h4>Enter Details</h4>
                                    <form name="frminput" autocomplete="off">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="firstname" >First Name *</label>
                                                    <div class="input-group">
                                                        <input id="firstname" type="text" class="form-control" name="firstname" required/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="lastname" >Last Name *</label>
                                                    <div class="input-group">
                                                        <input id="lastname" type="text" class="form-control" name="lastname"  required/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="email" >Email *</label>
                                                    <div class="input-group">
                                                        <input id="email" type="email" class="form-control" name="email" required />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" className="btn btn-primary" onClick={this.setEventType} data-next-step="step4">Schedule Event</button>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className={(this.state.step=="step4"?"d-block":"d-none")}>
                            <div class="row justify-content-center">
                                <div class="col-md-8 text-center" >
                                    <h4>Confirmed</h4>
                                    <p class="text-muted">You are scheduled with {this.props.userinfo.name}</p>
                                </div>

                                <div class="row justify-content-center">
                                    <div class="list-group spacing-top col-md-8 schedule-list">
                                        
                                            
                                                
                                        <a href="javascript:;" className="list-group-item list-group-item-action">
                                            <i class="fa fa-circle fa-lg float-left item-bullet"></i>
                                            
                                            {this.state.event_name} 
                                            <p class="text-green"><i class="fa fa-calendar-o"></i>&nbsp;{this.state.start} - {this.state.end} {this.state.date_display}</p>
                                        </a>
                                    </div>  

                                    <div class="col-md-8 spacing-top">
                                        <a href="javascript:;" onClick={this.setEventType} data-next-step="step1"><i class="fa fa-arrow-right"></i>&nbsp;Schedule Another Event</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
        }
    </script>

    

    <div class="container spacing-top">
       
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body" id="appt_app" style="min-height:500px;">
                    </div>
                </div>
            </div>
        </div>
       
    </div>
    
  
    <script type="text/babel">
        ReactDOM.render(<Appointment userinfo="{{  json_encode($user) }}" slotcheck_url="{{ route("appointment.check-slot",$enc_key) }} "freeslot_url="{{ route("appointment.free-slot",$enc_key) }}" book_url="{{ route("appointment.book",$enc_key) }}" event_types="{{  json_encode($event_types) }}" />, document.getElementById("appt_app"));
    </script>
@endsection