<div>
    <div>{{ $date_display }}</div>
    <div style="max-height:500px;overflow-y:auto;">
        @if(count($freeslots) > 0)
            @foreach ($freeslots as $freeslot)
                
                @php 
                    $date_slot_till = date_create_from_format("h:i a",$freeslot);
                    $date_slot_till = date_modify($date_slot_till,"+".$duration."mins");

                  
                    $date_slot_till = date_format($date_slot_till,"h:i a");
                @endphp
                
                <a href="javascript:;" data-start="{{ $freeslot }}" data-duration="{{ $duration }}" data-end="{{ $date_slot_till }}" data-date-display="{{ $date_display }}" style="font-size:12px;" class="btn-block btn btn-flat">{{ $freeslot }}</a>
            @endforeach
        @else   
            <div class="text-center">Sorry! no slots available</div>
        @endif
    <div>
</div>