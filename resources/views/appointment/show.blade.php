@php $date_str = "" @endphp 
@if(count($appointments) > 0)             
    @foreach($appointments as $appointment) 
        
        @php
            if($date_str != $appointment->date)
            {
                $date_str = $appointment->date;
        @endphp
                <div class="card event-date">
                    <div class="card-body ">
                        @php echo date("l,d F Y",strtotime($date_str)) @endphp
                    </div>
                </div>
        @php
            }
        @endphp                             
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <i class="fa fa-circle fa-lg item-bullet float-left" style="margin-top:3px;margin-right:10px;"></i>
                        <div>{{ date("h:ia",strtotime($appointment->start)) }} - {{ date("h:ia",strtotime($appointment->end)) }}</div>
                    </div>
                    <div class="col-md-9">
                        <div class="float-right"><i class="fa fa-envelope text-primary">&nbsp;</i><a href="mailto:{{ $appointment->email }}">{{ $appointment->email }}</a></div>
                        <div>
                            <div><strong>{{ $appointment->firstname }} {{ $appointment->lastname }}</strong></div>
                            <div><span>Event Type</span> <strong>{{ $appointment->event_type()->first()->event_name }}</strong></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    
@else
    <div class="card">
        <div class="card-body text-center">
            <i class="fa fa-exclamation-triangle fa-lg"></i>
            <div class="text-muted">No Events Available</div>
        </div>
    <div>
@endif 
 