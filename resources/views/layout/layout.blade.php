<!DOCTYPE html>
<html>
    <head>
        <title>{{ env("APP_NAME") }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        @include("common.asset")
    </head>
    <body>
        @include("common.appnav")
        @yield('content')
    </body>
</html>