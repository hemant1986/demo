

<div class="card">
    <div class="card-body" style="padding-bottom:0px;">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h4>My Schedule</h4>
                
                <ul class="nav nav-tabs" style="margin-bottom:0px;margin-top:10px;">
                    <li class="nav-item">
                        @php $active="" @endphp 
                        @if($view_dashboard=="view_events")
                            @php $active="active" @endphp
                        @endif 
                        <a class="nav-link {{$active}}" href="{{ route("home","view_dashboard=view_events") }}">Event Types</a>
                    </li>
                    <li class="nav-item">
                        @php $active="" @endphp
                        @if($view_dashboard=="view_scheduled_events")
                            @php $active="active" @endphp
                        @endif 
                        <a class="nav-link {{$active}}" href="{{ route("home","view_dashboard=view_scheduled_events") }}">Scheduled Events</a>
                    </li>
                
                </ul>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>