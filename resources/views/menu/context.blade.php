@if($context == "dashboard")
<div class="card contextmenu dashboard">
    <div class="card-body">
        <div class="row justify-content-center">
            
            <div class="col-md-8">
                <h4>My Schedule&nbsp;<i class="fa fa-chevron-down"></i></h4>
                
                <ul class="nav nav-tabs" style="margin-bottom:0px;margin-top:10px;">
                    <li class="nav-item">
                        @php $active="" @endphp 
                        @if($view_dashboard=="event_type")
                            @php $active="active" @endphp
                        @endif 
                        <a class="nav-link {{$active}}" href="{{ route("home","view_dashboard=event_type") }}">Event Types</a>
                    </li>
                    <li class="nav-item">
                        @php $active="" @endphp
                        @if($view_dashboard=="scheduled_events")
                            @php $active="active" @endphp
                        @endif 
                        <a class="nav-link {{$active}}" href="{{ route("home","view_dashboard=scheduled_events") }}">Scheduled Events</a>
                    </li>
                
                </ul>
            </div>
            
        </div>
    </div>
</div>
@elseif($context == "event_type")
    <div class="card contextmenu">
        <div class="card-body">
            <div class="row justify-content-center">
                
                <div class="col-md-8 ">
                    <div class="float-left"><a href="{{ route("home") }}" class="btn btn-flat"><i class='fa fa-chevron-left'></i>Back</a></div>

                    <h4 class="text-center">
                    @if(isset($event_type->recid))
                        Edit Event Type
                    @else
                        Add Event Type
                    @endif
                    </h4>
                </div>
                
            </div>
        </div>
    </div>
@endif 

