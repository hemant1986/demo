var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
//var short_weekday = ["sun","mon","tue","wed","thu","fri","sat"];
var display_weekday_order = ["mon","tue","wed","thu","fri","sat","sun"];

var active_date = null;
var current_date = new Date();
current_date.setHours(0);
current_date.setMinutes(0);
current_date.setSeconds(0);
current_date.setMilliseconds(0);

function makeCalendar(appointment_reactref,date)
{
    
    if(date) {}
    else
    {
        date = new Date();
    }

    active_date = date;

    hide_timeslot();
    $("#calendar").empty();

    $("#calendar").before("<div class='toast app-toast' data-autohide='false'><div class='toast-body' ><i class='fas fa-cog fa-spin'></i>&nbsp;Checking Calendar...</div></div>");

    var $title = $("<h3>Select Date / Time</h3>")
    var $dateline = $("<div class='calendar-nav'><div class='float-right'><a href='javascript:;' data-calendar-nav='true' data-calendar-prev='true'><i class='fa fa-angle-left'></i></a><a href='javascript:;' data-calendar-nav='true' data-calendar-next='true'><i class='fa fa-angle-right'></i></a></div><div>"+months[date.getMonth()]+" "+date.getFullYear()+"</div></div>");
    var $calendar_slots = $("<div data-calendar-slot='true'></div>");

    $("#calendar").append($title).append($dateline).append($calendar_slots);

    var $table = $("<table width='100%' cellpadding='8'></table>");
    var $table_header = $("<thead></thead");
    var $table_body = $("<tbody></tbody>");


    $table.append($table_header);
    $table.append($table_body);

    var $tr = $("<tr></tr>")
    for(i=0;i<display_weekday_order.length;i++)
    {
        $tr.append("<th class='text-uppercase text-muted' style='font-weight:normal;'>"+display_weekday_order[i]+"</th>");
    }

    $tr.appendTo($table_header);


    $table.appendTo($calendar_slots);

    //CODE TO SETUP DATES 

    var first_date = 1;
    var next_date = new Date(date.getFullYear(),date.getMonth()+1,1);
    next_date.setDate(next_date.getDate()-1);
    var last_date = next_date.getDate();

    var $tr = $("<tr></tr>");
    $tr.appendTo($table_body);

    var iterator_date = new Date(date.getFullYear(),date.getMonth(),1,0,0,0,0);

    for(i=first_date;i<=last_date;i++)
    {
        var weekday = iterator_date.getDay();
        if(weekday == 0)
        {
            weekday = 6;
        }
        else
        {
            weekday--;
        }
        if(weekday == 0)
        {
            //console.log("creating new weekday " + weekday);
            $tr = $("<tr></tr>");
            $tr.appendTo($table_body);
        }

        //  console.log(weekday);

        if(i == 1)
        {
            if(weekday > 0)
            {
                //POLY FILL THE TR
                $tr.append("<td  style='text-align:right;' colspan='"+(weekday)+"'></td>");
            }
        }

        var clickable_slot = "";
        if( (weekday == 5) || (weekday == 6) )
        {

        }
        else
        {
            if(iterator_date >= current_date)
            {
                clickable_slot = "slot";
            }
        }
        
        $tr.append("<td align='center' valign='middle' ><div class='"+clickable_slot+"' data-date='"+iterator_date.getFullYear()+"-"+(iterator_date.getMonth() + 1)+"-"+iterator_date.getDate()+"'>"+iterator_date.getDate()+"</div></td>");

        //console.log($tr.children());
        
        
        
    
        iterator_date.setDate(iterator_date.getDate()+1)
    }

    bindCalendarEvents(appointment_reactref,date);

    //checkAvailabilitySlotsForCalendar(appointment_reactref,date); //LATENCY HENCE SKIPPING SLOT CHECKING
}

function checkAvailabilitySlotsForCalendar(appointment_reactref,date)
{
   
    $("#calendar").prev().toast('show');

    $.ajax({
        url:appointment_reactref.props.slotcheck_url,
        data:{date:date.getFullYear()+"-"+(date.getMonth() + 1)+"-"+date.getDate()},
        cache:false,
        dataType:"json",
        success:function(response)
        {
            $("#calendar").prev().toast('hide');
            if(response.notavailable)
            {
             
                for(i=0;i<response.notavailable.length;i++)
                {
                    //console.log("[data-date="+response.notavailable[i]+"]");
                    $("#calendar").find("[data-date='"+response.notavailable[i]+"']").removeClass("slot");
                }
            }

            bindCalendarEvents(appointment_reactref,date);
        }
    })
}

function display_timeslot()
{
    $("#calendar").removeClass().addClass("col-md-8").next().removeClass("d-none");
}
function hide_timeslot()
{
    $("#calendar").removeClass().addClass("col-md-12").next().addClass("d-none");
}

function bindCalendarEvents(appointment_reactref,date)
{
    $("#calendar .slot").off("click").on("click",function()
    {
      
        display_timeslot();

        $("#calendar_timeslot").html("<div class='text-center'><i class='fa fa-cog fa-spin'></i></div>");

        $("#calendar .active").removeClass("active");
        $(this).addClass("active");
        var date = $(this).attr("data-date");

        
        $.ajax({
            url:appointment_reactref.props.freeslot_url,
            data:{date:date,event_min:appointment_reactref.state.event_min},
            cache:false,
            success:function(response)
            {
                $("#calendar_timeslot").html(response).find("[data-start]").off("click").on("click",function()
                {
                    
                    appointment_reactref.setEventTimeSlot(this,date);
                })
            }
        })
    })

    $("#calendar [data-calendar-nav]").off("click").on("click",function()
    {
        active_date.setDate(1);

        if($(this).attr("data-calendar-prev") == "true")
        {
            active_date.setDate(active_date.getDate()-1);
        }
        else
        {
            active_date.setMonth(active_date.getMonth() + 1);
        }
        

        makeCalendar(appointment_reactref,active_date);
    })
}